```plantuml
  
  'SETTINGS
  '---
  'scale 720 width
  left to right direction
  
  'Title custom style
  skinparam titleBorderRoundCorner 0
  skinparam titleBorderThickness 1
  skinparam titleBackgroundColor red
  skinparam titleFontColor white
  skinparam titleFontSize 24
  
  'legend custom style
  skinparam legendBorderRoundCorner 0
  skinparam legendBorderThickness 0
  skinparam legendFontSize 14
  skinparam legendFontColor white
  skinparam legendBackgroundColor darkGrey
  
  'HEADER
  '---
  title
  <font color=white> Frontend : Class diagram </font>
  end title
  
  legend top right
  <font color=red>Objective : </font> Overview of the system's structure.
  end legend
  
  
  'BODY
  '---
  class "feature-routing.module" as featureRouterModule
  
  class "featureComponent" as featureComponent{
      currentModel: FeatureModel
      service: FeatureService
      ---
      ngOnInit()
  }
  class "featureModel" as featureModel
  interface "IDataJson" as IDataJson #lightGreen
  class "featureService" as featureService {
      Interface: <font color=Green>IDataJson</font>
      Model: featureModel
      ---
      getFeature(<font color=Green>params: IdParam</font>) : Observable<IDataJson>
      transformFeature(<font color=Green>params: IDataJson</font>) : featureModel
  }
  
  
  
  featureComponent --o featureRouterModule
  featureModel --o featureComponent
  featureModel -right-* featureService
  IDataJson -left-* featureService
  featureService --o featureComponent
  
  ```
