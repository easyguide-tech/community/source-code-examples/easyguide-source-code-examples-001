```plantuml
  'SETTINGS
  '---
  'scale 720 width
  'left to right direction
  
  'Title custom style
  skinparam titleBorderRoundCorner 0
  skinparam titleBorderThickness 1
  skinparam titleBackgroundColor red
  skinparam titleFontColor white
  skinparam titleFontSize 24
  
  'legend custom style
  skinparam legendBorderRoundCorner 0
  skinparam legendBorderThickness 0
  skinparam legendFontSize 14
  skinparam legendFontColor white
  skinparam legendBackgroundColor darkGrey
  
  'HEADER
  '---
  title
  <font color=white> Example : Activity diagram </font>
  end title
  
  legend top right
  <font color=red>Objective : </font> Overview of the system's activity
  end legend
  
  
  'BODY
  '---
  
  start
  :ClickServlet.handleRequest();
  :new page;
  if (Page.onSecurityCheck) then (true)
    :Page.onInit();
    if (isForward?) then (no)
    :Process controls;
    if (continue processing?) then (no)
      stop
    endif
  
    if (isPost?) then (yes)
      :Page.onPost();
    else (no)
      :Page.onGet();
    endif
    :Page.onRender();
    endif
  else (false)
  endif
  
  if (do redirect?) then (yes)
    :redirect process;
  else
    if (do forward?) then (yes)
    :Forward request;
    else (no)
    :Render page template;
    endif
  endif
  
  stop
  
  ```
