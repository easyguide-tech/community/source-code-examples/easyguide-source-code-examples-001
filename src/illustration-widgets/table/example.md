| head left   | title centered |   title right |
| :---------- | :------------: | ------------: |
| row         |     value      |         value |
| another row | another value  | another value |
