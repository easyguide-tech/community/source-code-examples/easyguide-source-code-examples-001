---
output:
  custom_document:
    toc: true
    toc_depth: 2
    path: ./exports/document.epub
    pandoc_args: ["--dpi=300", "--highlight-style=tango","--metadata=keywords:'document'"]
---


# Pandoc EPUB

```bash
pandoc --from markdown --to epub  -o "./exports/document.epub"  --filter pandoc-plantuml-filter  "sample.md"
```
