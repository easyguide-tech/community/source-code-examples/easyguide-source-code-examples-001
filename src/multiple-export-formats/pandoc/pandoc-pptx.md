---
output:
  custom_document:
    toc: true
    toc_depth: 2
    path: ./exports/document.pptx
    pandoc_args: ["--dpi=300", "--highlight-style=tango",--metadata=keywords:'document'"]
---

# Pandoc PPTX

```bash
pandoc --from markdown -o "./exports/document.pptx"  --filter pandoc-plantuml-filter  "sample.md"
```
