---
output:
  pdf_document:
    toc: true
    toc_depth: 2
    path: ./exports/document.pdf
    latex_engine: pdflatex
    pandoc_args: ["--pdf-engine=pdflatex",  "--dpi=300", "--highlight-style=tango", "--metadata=keywords:'document'"]
---


# Pandoc PDF

```bash
pandoc --from markdown --to html5  -o "./exports/document.pdf"  --filter pandoc-plantuml-filter  "sample.md" --pdf-engine pdflatex
```
